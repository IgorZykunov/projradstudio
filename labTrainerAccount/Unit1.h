//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TButton *buReset;
	TButton *buAbout;
	TLayout *Layout1;
	TGridPanelLayout *GridPanelLayout1;
	TRectAnimation *RectAnimation1;
	TRectangle *Rectangle1;
	TRectangle *Rectangle2;
	TLabel *laCode;
	TLabel *Label2;
	TGridPanelLayout *GridPanelLayout2;
	TRectAnimation *RectAnimation2;
	TButton *buYes;
	TButton *buNo;
	TLabel *laCorrect;
	TLabel *laWrong;
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buResetClick(TObject *Sender);
	void __fastcall buYesClick(TObject *Sender);
	void __fastcall buNoClick(TObject *Sender);
private:
  int FCountCorrect;
  int FCountWrong;
  bool FAnswerCorrect;
  void DoReset();
  void DoContinue();
  void DoAnswer (bool aValue);
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
