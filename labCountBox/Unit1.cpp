//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "uUtils.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	FListBox = new TList;
	for (int i=1; i <= cMaxBox; i++){
        FListBox->Add(this->FindComponent("Rectangle"+IntToStr(i)));
	}
	FListAnswer = new TList;
	FListAnswer->Add(Button1);
	FListAnswer->Add(Button2);
	FListAnswer->Add(Button3);
	FListAnswer->Add(Button4);
	FListAnswer->Add(Button5);
	FListAnswer->Add(Button6);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormDestroy(TObject *Sender)
{
	delete FListBox;
    delete FListAnswer;
}
//---------------------------------------------------------------------------
void TForm1::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimeValue = Time ().Val + (double)30/(24*60*60);//30 ���
	tmPlay->Enabled = true;
	DoContinue();
}
void TForm1::DoContinue()
{
	for (int i=0; i< cMaxBox; i++) {
	((TRectangle*)FListBox->Items[i])->Fill->Color = TAlphaColorRec::Lightgray;
	}
	FNumberCorrect = RandomRange(cMinPossible,cMaxPossible);
	int *x = RandomArrayUnique(cMaxBox, FNumberCorrect);
	for (int i = 0; i<cMaxCorrect; i++) {
		((TRectangle*)FListBox->Items(x[i])->Fill->Color= TAlphaColorRec::Lightblue;
	}
	int xAnswerStart = FNumberCorrect - Random(cMaxAnswer-1);
	if (xAnswerStart < cMinPossible)
		xAnswerStart = cMinPossible;
	}
	for (int i=0; i< cMaxAnswer;i++) {
        ((TButton*)FListAnswer->Items[i])->Text = IntToStr(xAnswerStart+i);
	}
}
void TForm1::DoAnswer(int aValue)
{
	(aValue == FNumberCorrect) ? FCountCorrect++ : FCountWrong++;
	if (FCountWrong > 5) (/*�������� ���*/)
    DoContinue();
	}
}
void TForm1::DoFinish()
{
	tmPlay->Enabled = false;
}

void __fastcall TForm1::Button2Click(TObject *Sender)
{
    DoAnswer(StrToInt(((TButton*)Sender)->Text));
}
//---------------------------------------------------------------------------

void __fastcall TForm1::tmPlayTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	Label2->Text = FormatDateTime("nn:ss", x);
	if (x<=0)
        DoFinish();
}
//---------------------------------------------------------------------------

