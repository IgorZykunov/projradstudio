//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;

void LoadResourceToImage (UnicodeString aResName, TBitmap* aResult)
{
	TResourceStream* x;
	x = new TResourceStream((int)HInstance, aResName, RT_RCDATA);
	try {
        aResult->LoadFromStream(x);
	}
	__finally {
		delete x;
    }
}
//---------------------------------------------------------------------------
UnicodeString LoadResourceToText (UnicodeString aResName)
{
	TResourceStream* x;
	TStringStream* xSS;
	x = new TResourceStream((int)HInstance, aResName, RT_RCDATA);
	try {
         xSS= new TStringStream("",TEncoding::UTF8, true);

		try {
			xSS ->LoadFromStream(x);
			return xSS->DataString;
		}
		__finally {
		delete xSS;
		}  }
	__finally {
	delete x;
	}

}//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	LoadResourceToImage("JpgImage_1",im->Bitmap);
	me->Lines->Text = LoadResourceToText ("Resource_1");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	LoadResourceToImage("JpgImage_2",im->Bitmap);
	me->Lines->Text = LoadResourceToText ("Resource_2");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	im->Bitmap->SetSize(0,0);
    me->Lines->Clear();
}

__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}

//---------------------------------------------------------------------------
