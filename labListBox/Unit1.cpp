//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
   // ��� ���� ��� ���� ��������� ����
const UnicodeString cPath = System::Ioutils::TPath::GetDocumentsPath();

const UnicodeString cList[] = {
	"coupe","hardtop","hatchback","liftback",
	"limousine","minibus","minivan","sedan","universal"
} ;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void Tfm::LoadItem(int aIndex)
{
	UnicodeString xName = System::Ioutils::TPath::Combine(cPath,cList[aIndex]);
	im->Bitmap->LoadFromFile (xName+".jpg");
	TStringList *x = new TStringList;
	__try {
		x->LoadFromFile(xName+".txt");
		tx->Text = x->Text;
	}
	__finally {
		x->DisposeOf();
	}
}




void __fastcall Tfm::Button1Click(TObject *Sender)
{
    tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
    tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::ListBox1ItemClick(TCustomListBox * const Sender, TListBoxItem * const Item)

{
	sbItem->ViewportPosition = TPointF (0,0);
	LoadItem(Item->Index);
	tx->RecalcSize();
    tc->ActiveTab = tiItem;
}
//---------------------------------------------------------------------------
