//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void __fastcall TForm1::MouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, float X, float Y)
{
	FX = X;
	FY = Y;
	FIsDragging = true;
	((TControl *)Sender)->BringToFront();
	((TControl *)Sender)->Root->Captured = interface_cast<IControl>(Sender);
	if (dynamic_cast<TShape*>(Sender))
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Lightblue;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::MouseEnter(TObject *Sender)
{
if (dynamic_cast<TShape*>(Sender))
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Yellow;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::MouseLeave(TObject *Sender)
{
    if (dynamic_cast<TShape*>(Sender))
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Lightgrey;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::MouseMove(TObject *Sender, TShiftState Shift,
          float X, float Y)
{
	if (FIsDragging && Shift.Contains(ssLeft)) {
	((TControl *)Sender)->Position->X += X - FX;
	((TControl *)Sender)->Position->Y += Y - FY;
	if (dynamic_cast<TShape*>(Sender))
		((TShape *)Sender)->Fill->Color = TAlphaColorRec::Lightpink;
	}
}
//---------------------------------------------------------------------------
