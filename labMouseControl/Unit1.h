//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
#include <FMX.StdCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TRectangle *Rectangle1;
	TCircle *Circle1;
	TRoundRect *RoundRect1;
	TImageControl *ImageControl1;
	void __fastcall FormMouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          float X, float Y);
	void __fastcall Rectangle1MouseEnter(TObject *Sender);
	void __fastcall ImageControl1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  float X, float Y);
	void __fastcall ImageControl1MouseEnter(TObject *Sender);
	void __fastcall ImageControl1MouseLeave(TObject *Sender);
	void __fastcall ImageControl1MouseMove(TObject *Sender, TShiftState Shift, float X,
		  float Y);
private:
	float FX,FY;
    bool FIsDragging;
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
