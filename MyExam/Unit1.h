//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Colors.hpp>
#include <FMX.ListBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMenu;
	TTabItem *tiGame;
	TTabItem *tiSettings;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buGame;
	TButton *buSettings;
	TButton *buAbout;
	TButton *buHelp;
	TTabItem *tiHelp;
	TLabel *Label1;
	TRectangle *ReBackground;
	TToolBar *ToolBar1;
	TLabel *laTimer1;
	TButton *Button1;
	TLabel *Label3;
	TGridPanelLayout *GridPanelLayout2;
	TRectangle *Rectangle2;
	TRectangle *Rectangle3;
	TLabel *Label4;
	TGridPanelLayout *GridPanelLayout3;
	TButton *buNo;
	TButton *buYes;
	TGridPanelLayout *GridPanelLayout4;
	TLabel *Label5;
	TColorComboBox *ColorComboBox1;
	TLabel *Label6;
	TComboBox *ComboBox1;
	TToolBar *ToolBar2;
	TLabel *Label7;
	TButton *Button2;
	TToolBar *ToolBar3;
	TLabel *Label8;
	TButton *Button3;
	TTimer *Timer2;
	TTimer *Timer1;
	TLabel *laTimer2;
	void __fastcall buAllMouseEnter(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
	void __fastcall buGameClick(TObject *Sender);
	void __fastcall buSettingsClick(TObject *Sender);
	void __fastcall buHelpClick(TObject *Sender);
	void __fastcall buAllMouseLeave(TObject *Sender);
	void __fastcall buSettingsMouseEnter(TObject *Sender);
	void __fastcall buAboutMouseEnter(TObject *Sender);
	void __fastcall buHelpMouseEnter(TObject *Sender);
	void __fastcall buSettingsMouseLeave(TObject *Sender);
	void __fastcall buAboutMouseLeave(TObject *Sender);
	void __fastcall buHelpMouseLeave(TObject *Sender);
	void __fastcall FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
		  TShiftState Shift);
    void __fastcall buAnswerClick(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);


private:	// User declarations
	int Right;
	int Bad;
	bool Correct;

	void Reset();
	void Rand();
	void Choise(int aTag);

	int PTimeToPlay;  // ������ 321
	int PPlayTime;  // ������ ����
    int Settings;//���������


public:
	// User declarations
	__fastcall TForm1(TComponent* Owner);
};
const UnicodeString colorName[] = {"�����", "�������", "�������", "������", "���������", "����������", "������", "�����", "�����"};

const TAlphaColor colorCode[] = {TAlphaColorRec::Blue, TAlphaColorRec::Red, TAlphaColorRec::Green, TAlphaColorRec::Yellow, TAlphaColorRec::Orange, TAlphaColorRec::Brown, TAlphaColorRec::Black, TAlphaColorRec::White, TAlphaColorRec::Gray,};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
