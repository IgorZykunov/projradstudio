//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TForm1::Reset(){
// laTimer1 3 2 1
// laTimer2 ������ ������
	Right = 0;    //����������
	Bad = 0;
	PTimeToPlay = 3; // ������� �� ������
	PPlayTime = PPlayTime;
	laTimer2->Text = PPlayTime;
	laTimer1->Text = "���� �������� �����: " + IntToStr(PTimeToPlay);
	laTimer1->Visible = true;
	laTimer2->Enabled = true;
	Timer2->Enabled = false;
	Label4->Text = "����";
	Rectangle2->Fill->Color = TAlphaColorRec::Azure;//���� ����
}
//---------------------------------------------------------------------------
void TForm1::Rand(){
	int rand = Random(9);

	if(rand == 1){
		int colorId = Random(9);
		Label4->Text = colorName[colorId];
		Rectangle2->Fill->Color = colorCode[colorId];
		Correct = true;
	}else{
		Correct = false;
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAllMouseEnter(TObject *Sender)
{
	TButton *bu = (TButton*) Sender;
	bu->TextSettings->Font->Size += 6;
	bu->Margins->Rect = TRect(0,0,0,0);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAboutClick(TObject *Sender)
{
    ShowMessage ("�����: ����� �������");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buGameClick(TObject *Sender)
{
    Reset();
    tc->GotoVisibleTab(tiGame->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buSettingsClick(TObject *Sender)
{
     tc->GotoVisibleTab(tiSettings->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buHelpClick(TObject *Sender)
{
     tc->GotoVisibleTab(tiHelp->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAllMouseLeave(TObject *Sender)
{
	TButton *bu = (TButton*) Sender;
	bu->TextSettings->Font->Size -= 6;
	bu->Margins->Rect = TRect(5,5,5,5);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buSettingsMouseEnter(TObject *Sender)
{
	TButton *bu = (TButton*) Sender;
	bu->TextSettings->Font->Size += 6;
	bu->Margins->Rect = TRect(0,0,0,0);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAboutMouseEnter(TObject *Sender)
{
	TButton *bu = (TButton*) Sender;
	bu->TextSettings->Font->Size += 6;
	bu->Margins->Rect = TRect(0,0,0,0);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buHelpMouseEnter(TObject *Sender)
{
	TButton *bu = (TButton*) Sender;
	bu->TextSettings->Font->Size += 6;
	bu->Margins->Rect = TRect(0,0,0,0);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buSettingsMouseLeave(TObject *Sender)
{
	TButton *bu = (TButton*) Sender;
	bu->TextSettings->Font->Size -= 6;
	bu->Margins->Rect = TRect(5,5,5,5);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buAboutMouseLeave(TObject *Sender)
{
	TButton *bu = (TButton*) Sender;
	bu->TextSettings->Font->Size -= 6;
	bu->Margins->Rect = TRect(5,5,5,5);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buHelpMouseLeave(TObject *Sender)
{
	TButton *bu = (TButton*) Sender;
	bu->TextSettings->Font->Size -= 6;
	bu->Margins->Rect = TRect(5,5,5,5);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormKeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift)
{
     if(tc->ActiveTab != tiMenu && Key == vkHardwareBack){
	   tc->GotoVisibleTab(tiMenu->Index);
       Key = 0;
    }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	tc->GotoVisibleTab(tiMenu->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Randomize();
	PPlayTime = 10; //
	tc->ActiveTab = tiMenu;
}
//---------------------------------------------------------------------------
 void __fastcall TForm1::buAnswerClick(TObject *Sender)
{

	TButton *bu = (TButton*) Sender;
   Choise(bu->Tag);


}
//---------------------------------------------------------------------------

void TForm1::Choise(int aTag){
	 if(aTag == Correct) Right++;
	 else Bad++;
	 Rand();

}
//---------------------------------------------------------------------------


