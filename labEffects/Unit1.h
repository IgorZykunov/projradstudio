//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Effects.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Filter.Effects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TButton *Button6;
	TShadowEffect *ShadowEffect1;
	TBlurEffect *BlurEffect1;
	TGlowEffect *GlowEffect1;
	TInnerGlowEffect *InnerGlowEffect1;
	TBevelEffect *BevelEffect1;
	TReflectionEffect *ReflectionEffect1;
	TImage *Image1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image5;
	TImage *Image6;
	TBlurEffect *BlurEffect2;
	TImage *Image7;
	TBlurEffect *BlurEffect3;
	TImage *Image8;
	TBlurEffect *BlurEffect4;
	TSwirlEffect *SwirlEffect1;
	TTilerEffect *TilerEffect1;
	TToonEffect *ToonEffect1;
	TPixelateEffect *PixelateEffect1;
	TWaveTransitionEffect *WaveTransitionEffect1;
	TEmbossEffect *EmbossEffect1;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
