//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "labIndyTCP_SRV.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buStartClick(TObject *Sender)
{
	IdTCPServer->Active = true;
    me->Lines->Add("��� �������");
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buStopClick(TObject *Sender)
{
	IdTCPServer->Active = false;
	me->Lines->Add("���������� �������");
    Close();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::IdTCPServerConnect(TIdContext *AContext)
{
	me->Lines->Add(Format("[%s] - Client connected",
		ARRAYOFCONST ((AContext->Connection->Socket->Binding->PeerIP))
    ));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::IdTCPServerExecute(TIdContext *AContext)
{
	UnicodeString x = AContext->Connection->Socket->ReadLn();
	me->Lines->Add("������ ������� " + x);

	if (x == "���������") {
		AContext->Connection->Socket->WriteLn(edStr->Text,
			IndyTextEncoding_UTF8());
	}
	
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	IdTCPServer->Active = true;
    me->Lines->Add("��� �������");
}
//---------------------------------------------------------------------------




