//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::acConnectExecute(TObject *Sender)
{
	IdTCPClient1->Host = "127.0.0.1";
	IdTCPClient1->Port = StrToInt(8091);
	IdTCPClient1->Connect();
}
//---------------------------------------------------------------------------
void __fastcall Tfm::acDisconnectExecute(TObject *Sender)
{
	IdTCPClient1->Disconnect();
	Close();
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
void __fastcall Tfm::acGetStrExecute(TObject *Sender)
{
	IdTCPClient1->Socket->WriteLn("str");
	UnicodeString x;
	x = IdTCPClient1->Socket->ReadLn(IndyTextEncoding_UTF8());
    me->Lines->Add(x);
}
//---------------------------------------------------------------------------


void __fastcall Tfm::FormCreate(TObject *Sender)
{
	IdTCPClient1->Host = "127.0.0.1";
	IdTCPClient1->Port = StrToInt(8091);
	IdTCPClient1->Connect();
}
//---------------------------------------------------------------------------

