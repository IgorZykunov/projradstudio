//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.EditBox.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.NumberBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TFm : public TForm
{
__published:	// IDE-managed Components
	TToolBar *ToolBar1;
	TLabel *laCaption;
	TButton *buAbout;
	TLayout *Ly;
	TEdit *EdPassword;
	TButton *BuPassword;
	TCheckBox *ckLower;
	TCheckBox *ckUpper;
	TCheckBox *ckNumber;
	TCheckBox *ckSpec;
	TLabel *labLenght;
	TNumberBox *edLenght;
	void __fastcall BuPasswordClick(TObject *Sender);
	void __fastcall buAboutClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TFm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFm *Fm;
//---------------------------------------------------------------------------
#endif
