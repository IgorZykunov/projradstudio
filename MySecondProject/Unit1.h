//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.Effects.hpp>
#include <FMX.Filter.Effects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *welcome;
	TTabItem *q1;
	TTabItem *q2;
	TTabItem *q3;
	TTabItem *q4;
	TTabItem *q5;
	TTabItem *memo;
	TLabel *Label1;
	TLabel *Label2;
	TLayout *Layout1;
	TLabel *Label3;
	TButton *Button1;
	TLayout *Layout2;
	TButton *Button2;
	TButton *Button3;
	TButton *Button4;
	TButton *Button5;
	TLabel *Label4;
	TLayout *Layout3;
	TImage *Image2;
	TButton *Renault;
	TButton *Button7;
	TButton *Button8;
	TButton *Button9;
	TLabel *Label5;
	TLayout *Layout8;
	TImage *Image7;
	TButton *Lada;
	TButton *Button27;
	TButton *Button28;
	TButton *Button29;
	TLabel *Label10;
	TLayout *Layout9;
	TImage *Image8;
	TButton *Button30;
	TButton *Button31;
	TButton *Button32;
	TButton *Button33;
	TLabel *Label11;
	TLayout *Layout10;
	TImage *Image9;
	TButton *Button34;
	TButton *Button35;
	TButton *Button36;
	TButton *Button37;
	TLabel *Label12;
	TMemo *Memo1;
	TImage *Image1;
	TButton *Button6;
	TStyleBook *StyleBook1;
	TButton *Button10;
	TToolBar *ToolBar1;
	TBlurEffect *BlurEffect1;
	TBlurEffect *BlurEffect2;
	TBlurEffect *BlurEffect3;
	TBlurEffect *BlurEffect4;
	TBlurEffect *BlurEffect5;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
	void __fastcall RenaultClick(TObject *Sender);
	void __fastcall Button7Click(TObject *Sender);
	void __fastcall Button8Click(TObject *Sender);
	void __fastcall Button9Click(TObject *Sender);
	void __fastcall LadaClick(TObject *Sender);
	void __fastcall Button27Click(TObject *Sender);
	void __fastcall Button28Click(TObject *Sender);
	void __fastcall Button29Click(TObject *Sender);
	void __fastcall Button30Click(TObject *Sender);
	void __fastcall Button31Click(TObject *Sender);
	void __fastcall Button32Click(TObject *Sender);
	void __fastcall Button33Click(TObject *Sender);
	void __fastcall Button34Click(TObject *Sender);
	void __fastcall Button35Click(TObject *Sender);
	void __fastcall Button36Click(TObject *Sender);
	void __fastcall Button37Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button10Click(TObject *Sender);
private:	// User declarations
public:		double sum;// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
