//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TMyProject *MyProject;
//---------------------------------------------------------------------------
__fastcall TMyProject::TMyProject(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TMyProject::ListBoxItem1Click(TObject *Sender)
{
	tc->ActiveTab = Tab1;
}
//---------------------------------------------------------------------------

void __fastcall TMyProject::ListBoxItem2Click(TObject *Sender)
{
     tc->ActiveTab = Tab2;
}
//---------------------------------------------------------------------------

void __fastcall TMyProject::ListBoxItem3Click(TObject *Sender)
{
     tc->ActiveTab = Tab3;
}
//---------------------------------------------------------------------------

void __fastcall TMyProject::ListBoxItem4Click(TObject *Sender)
{
     tc->ActiveTab = Tab4;
}
//---------------------------------------------------------------------------

void __fastcall TMyProject::ListBoxItem5Click(TObject *Sender)
{
     tc->ActiveTab = Tab5;
}
//---------------------------------------------------------------------------

void __fastcall TMyProject::ListBoxItem11Click(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------

void __fastcall TMyProject::Button1Click(TObject *Sender)
{
	double volume, boost, min, max;
	volume = Edit1->Text.ToDouble();
	boost = Edit2->Text.ToDouble();
	min = 0.0435*volume*(1+boost);
	max = 0.0691*volume*(1+boost);
	lmin->Text= Format(L"Min %f hp", ARRAYOFCONST((min)));
	lmax->Text= Format(L"Max %f hp", ARRAYOFCONST((max)));

}
//---------------------------------------------------------------------------

void __fastcall TMyProject::Button2Click(TObject *Sender)
{
	ShowMessage ("�� ������ ������� �������������� ��������. ��� ������������� � ������� ������������� ���������� ��� ��������� �� ����������������� ������");
}
//---------------------------------------------------------------------------

void __fastcall TMyProject::Button3Click(TObject *Sender)
{
	ShowMessage ("������ ������� ������� ����������� ����������.") ;
}
//---------------------------------------------------------------------------

void __fastcall TMyProject::Button4Click(TObject *Sender)
{
	double volume, rpm, effic, result;
	volume = evolume->Text.ToDouble();
	rpm = erpm->Text.ToDouble();
	effic = eeffic->Text.ToDouble();
	result =  volume*rpm*effic*0.5/1000000;
	lresult->Text= Format(L" %f m^3", ARRAYOFCONST((result)));
}
//---------------------------------------------------------------------------

void __fastcall TMyProject::Button6Click(TObject *Sender)
{
	ShowMessage ("������ ������� ������� ��� �������") ;
}
//---------------------------------------------------------------------------

void __fastcall TMyProject::Button5Click(TObject *Sender)
{
	double volume, boost, result;
	volume = evolume2->Text.ToInt();
	boost = eboost2->Text.ToInt();
	result =  volume*(boost+1);
	lresult2->Text= Format(L" %f m^3", ARRAYOFCONST((result)));
}
//---------------------------------------------------------------------------

