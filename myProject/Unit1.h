//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Menus.hpp>
#include <FMX.Types.hpp>
#include <FMX.Ani.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.MultiView.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Objects.hpp>
#include <FMX.ActnList.hpp>
#include <System.Actions.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class TMyProject : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *Tab1;
	TTabItem *Tab2;
	TTabItem *Tab3;
	TTabItem *Tab4;
	TTabItem *Tab5;
	TMultiView *MultiView1;
	TListBox *ListBox1;
	TListBoxHeader *ListBoxHeader1;
	TListBoxItem *ListBoxItem1;
	TListBoxItem *ListBoxItem2;
	TListBoxItem *ListBoxItem3;
	TListBoxItem *ListBoxItem4;
	TListBoxItem *ListBoxItem5;
	TLabel *Label1;
	TRectangle *Rectangle3;
	TLabel *Label5;
	TRectangle *Rectangle4;
	TLabel *Label6;
	TActionList *ActionList1;
	TListBoxItem *ListBoxItem11;
	TMultiView *MultiView3;
	TListBox *ListBox3;
	TListBoxItem *ListBoxItem12;
	TListBoxItem *ListBoxItem13;
	TListBoxItem *ListBoxItem14;
	TListBoxItem *ListBoxItem15;
	TListBoxItem *ListBoxItem16;
	TListBoxItem *ListBoxItem17;
	TListBoxHeader *ListBoxHeader3;
	TLabel *Label7;
	TMultiView *MultiView2;
	TListBox *ListBox2;
	TListBoxItem *ListBoxItem6;
	TListBoxItem *ListBoxItem7;
	TListBoxItem *ListBoxItem8;
	TListBoxItem *ListBoxItem9;
	TListBoxItem *ListBoxItem10;
	TListBoxItem *ListBoxItem18;
	TListBoxHeader *ListBoxHeader2;
	TLabel *Label3;
	TMultiView *MultiView4;
	TListBox *ListBox4;
	TListBoxItem *ListBoxItem19;
	TListBoxItem *ListBoxItem20;
	TListBoxItem *ListBoxItem21;
	TListBoxItem *ListBoxItem22;
	TListBoxItem *ListBoxItem23;
	TListBoxItem *ListBoxItem24;
	TListBoxHeader *ListBoxHeader4;
	TLabel *Label8;
	TMultiView *MultiView5;
	TListBox *ListBox5;
	TListBoxItem *ListBoxItem25;
	TListBoxItem *ListBoxItem26;
	TListBoxItem *ListBoxItem27;
	TListBoxItem *ListBoxItem28;
	TListBoxItem *ListBoxItem29;
	TListBoxItem *ListBoxItem30;
	TListBoxHeader *ListBoxHeader5;
	TLabel *Label9;
	TRectangle *Rectangle5;
	TLabel *Label10;
	TGridPanelLayout *GridPanelLayout1;
	TLabel *Label11;
	TEdit *Edit1;
	TLabel *Label12;
	TEdit *Edit2;
	TButton *Button1;
	TLabel *lmin;
	TLabel *lmax;
	TLayout *Layout1;
	TButton *Button2;
	TLayout *Layout2;
	TRectangle *Rectangle6;
	TLabel *Label13;
	TGridPanelLayout *GridPanelLayout2;
	TLabel *Label14;
	TEdit *evolume;
	TLabel *Label15;
	TEdit *erpm;
	TLabel *Label16;
	TLabel *Label17;
	TLabel *lresult;
	TEdit *eeffic;
	TButton *Button3;
	TButton *Button4;
	TLayout *Layout4;
	TRectangle *Rectangle2;
	TLabel *Label21;
	TGridPanelLayout *GridPanelLayout4;
	TLabel *Label22;
	TEdit *evolume2;
	TLabel *Label23;
	TEdit *eboost2;
	TLabel *Label24;
	TLabel *lresult2;
	TButton *Button5;
	TButton *Button6;
	void __fastcall ListBoxItem1Click(TObject *Sender);
	void __fastcall ListBoxItem2Click(TObject *Sender);
	void __fastcall ListBoxItem3Click(TObject *Sender);
	void __fastcall ListBoxItem4Click(TObject *Sender);
	void __fastcall ListBoxItem5Click(TObject *Sender);
	void __fastcall ListBoxItem11Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button4Click(TObject *Sender);
	void __fastcall Button6Click(TObject *Sender);
	void __fastcall Button5Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TMyProject(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TMyProject *MyProject;
//---------------------------------------------------------------------------
#endif
