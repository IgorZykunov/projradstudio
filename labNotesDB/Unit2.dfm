object dm: Tdm
  OldCreateOrder = False
  Height = 241
  Width = 356
  object FDConnection: TFDConnection
    Params.Strings = (
      
        'Database=C:\Users\user\Documents\Embarcadero\Studio\Projects\lab' +
        'NotesDB\database.db'
      'DriverID=SQLite')
    Connected = True
    LoginPrompt = False
    AfterConnect = FDConnectionAfterConnect
    BeforeConnect = FDConnectionBeforeConnect
    Left = 40
    Top = 32
  end
  object taNotes: TFDTable
    Connection = FDConnection
    UpdateOptions.UpdateTableName = 'Notes'
    TableName = 'Notes'
    Left = 32
    Top = 120
    object taNotesCaption: TStringField
      FieldName = 'Caption'
      Origin = 'Caption'
      Required = True
      Size = 50
    end
    object taNotesPriority: TSmallintField
      FieldName = 'Priority'
      Origin = 'Priority'
      Required = True
    end
    object taNotesDetail: TStringField
      FieldName = 'Detail'
      Origin = 'Detail'
      Size = 300
    end
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'FMX'
    Left = 200
    Top = 48
  end
  object FDPhysSQLiteDriverLink1: TFDPhysSQLiteDriverLink
    Left = 232
    Top = 152
  end
end
