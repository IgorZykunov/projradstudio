//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	dm->taNotes->Edit();
	TabControl1->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
    TabControl1->ActiveTab = tiList;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::lvItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	dm->taNotes->Append();
	TabControl1->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
	dm->taNotes->Post();
	TabControl1->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
	dm->taNotes->Cancel();
	TabControl1->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button6Click(TObject *Sender)
{
    dm->taNotes->Delete();
	TabControl1->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------
