//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	Label1->Position->X = - Label1->Width;
	Label2->Position->X = this->Width + Label1->Width;
	Label3->Position->X = - Label3->Width;
	TAnimator::AnimateInt(Label1,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateInt(Label2,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateInt(Label3,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button2Click(TObject *Sender)
{
	laSample->AutoSize = true;
	laSample->TextSettings->Font->Size = 400;
	TAnimator::AnimateIntWait(laSample,"TextSettings.Font.Size",50,1,TAnimationType::Out, TInterpolationType::Linear);
	laSample->AutoSize = false;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button3Click(TObject *Sender)
{
	int y = GridPanelLayout1->Position->Y;
	GridPanelLayout1->Position->Y = - GridPanelLayout1->Height;
	TAnimator::AnimateIntWait(GridPanelLayout1,"Position.Y",y,1.65,TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button4Click(TObject *Sender)
{
Label1->Position->X = - Label1->Width;
	Button10->Position->Y = this->Width + Button10->Width;
	Button11->Position->Y = this->Width + Button11->Width;
	Button12->Position->Y = this->Width + Button12->Width;
	Button13->Position->Y = this->Width + Button13->Width;
	Button14->Position->Y = this->Width + Button14->Width;
	TAnimator::AnimateInt(Button11,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateInt(Button12,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateInt(Button13,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateInt(Button14,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateInt(Button10,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
}
//---------------------------------------------------------------------------
