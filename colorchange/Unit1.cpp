//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
void Tform1::NewColor(TAlphaColor aValue)
{
	laValue->Text = AlphaColorToString(aValue);
	laRGB->Text = Format ("(%d,%d,%d)",
		ARRAYOFCONST((
			TAlphaColorRec(aValue).R,
			TAlphaColorRec(aValue).G,
			TAlphaColorRec(aValue).B
		))
	);
    fm->fill->Color = aValue;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buRandomClick(TObject *Sender)
{
	NewColor (TAlphaColorF::Create(Random(256), Random(256), Random(256))
    .ToAlphaColor());
}
//---------------------------------------------------------------------------
void __fastcall TForm1::buLightGreenClick(TObject *Sender)
{
    NewColor (TAlphaColor(TAlphaColorRec::Lightgreen));
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	Randomize;
	this->Fill->Kind = TBrushKind::Solid;
    buRandomClick(buRandom);
}
//---------------------------------------------------------------------------
