//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *menu;
	TTabItem *play;
	TTabItem *finish;
	TTabItem *help;
	TLayout *lyMenu;
	TLabel *Label1;
	TLabel *Label2;
	TGridPanelLayout *GridPanelLayout1;
	TButton *Button1;
	TButton *Button2;
	TButton *Button3;
	TLayout *Layout1;
	TButton *Button4;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TTimer *tm;
	TGridPanelLayout *GridPanelLayout2;
	TButton *Button5;
	TButton *Button6;
	TGridPanelLayout *GridPanelLayout3;
	TLabel *Label6;
	TRectangle *Rectangle1;
	TLabel *Label8;
	TRectangle *Rectangle2;
	TLabel *Label7;
	TLabel *Label9;
	TLayout *Layout2;
	TButton *Button7;
	TButton *Button8;
	TGridPanelLayout *GridPanelLayout4;
	TLabel *Label10;
	TLabel *Label11;
	TLabel *Label12;
	TToolBar *ToolBar1;
	TLabel *Label13;
	TButton *Button9;
	void __fastcall tmTimer(TObject *Sender);
private:
	int FCountCorrect;
	int FCountWrong;
	bool FAnswerCorrect;
	double FTimeValue;
	void DoReset();
	void DoContinue();
	void DoAnswer(bool aValue);
    void DoFinish();
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
