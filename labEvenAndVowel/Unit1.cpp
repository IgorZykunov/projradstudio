//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void TForm1::DoReset()
{
	FCountCorrect = 0;
	FCountWrong = 0;
	FTimeValue = Time().Val + (float)30/(24*60*60);//30 cek
	tm->Enabled = true;
	DoContinue();
}
void TForm1::DoContinue ()
{
	//code
}
void TForm1::DoAnswer (bool aValue)
{
	(aValue ==FAnswerCorrect) ? FCountCorrect++ : FCountWrong++;
    DoContinue();
}
void TForm1::DoFinish ()
{
	tm->Enabled = false;
	Label10->Text = Format (L"Правильных =%d",
		ARRAYOFCONST((FCountCorrect)));
	Label11->Text = Format (L"Неправильных =%d",
		ARRAYOFCONST((FCountCorrect)));
    tc->ActiveTab = finish;
}

void __fastcall TForm1::tmTimer(TObject *Sender)
{
	double x = FTimeValue - Time ().Val;
	Label3->Text = FormatDateTime ("nn:ss",x);
	if (x <=0)
		DoFinish();
}
//---------------------------------------------------------------------------

