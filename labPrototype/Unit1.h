//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Data.Bind.GenData.hpp>
#include <Data.Bind.ObjectScope.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.ImageList.hpp>
#include <System.Rtti.hpp>
#include <Fmx.Bind.GenData.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TabControl1;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TListView *ListView1;
	TPrototypeBindSource *PrototypeBindSource1;
	TLabel *Label1;
	TEdit *Edit1;
	TTrackBar *TrackBar1;
	TImageList *ImageList1;
	TBindingsList *BindingsList1;
	TLinkControlToProperty *LinkControlToPropertyText;
	TGlyph *Glyph1;
	TLabel *Label2;
	TLinkControlToProperty *LinkControlToPropertyText2;
	TLinkControlToProperty *LinkControlToPropertyImageIndex;
	TLinkListControlToField *LinkListControlToField1;
	TImage *Image1;
	TLinkPropertyToField *LinkPropertyToFieldBitmap;
	TEdit *Edit2;
	TEdit *Edit3;
	TLinkControlToField *LinkControlToField1;
	TLinkControlToField *LinkControlToField2;
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
