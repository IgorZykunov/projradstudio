//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
#include <System.Math.hpp>

//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TPanel *Panel1;
	TButton *Button1;
	TLayout *ly;
	TRoundRect *RoundRect1;
	TRoundRect *RoundRect2;
	TLabel *Label1;
	TLabel *Label2;
	TRectangle *Rectangle1;
	TLabel *Label3;
	TLabel *Label4;
	TProgressBar *ProgressBar1;
	TLabel *Label5;
	TEdit *Edit1;
	TButton *Button2;
	TTimer *Timer1;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall Edit1KeyDown(TObject *Sender, WORD &Key, System::WideChar &KeyChar,
          TShiftState Shift);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
private:	// User declarations
	int FCountCurrent;
	int FCountWrong;
	int FSecretNumber;
	void DoReset ();
	void DoContinue ();
	void DoAnswer ();
	void DoViewQuestion(bool,aValue);

public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
